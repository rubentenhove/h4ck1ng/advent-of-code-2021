use bitvec::prelude::BitVec;

#[aoc_generator(day3)]
fn input_generator(input: &str) -> Vec<String> {
    input.lines().map(|l| l.to_string()).collect()
}

fn bitvec_to_num(input: BitVec<usize>) -> u32 {
    // Convert the result to a binary string, removing surrounding brackets
    let intermediate = format!("{:b}", input).replace(&['[', ']'][..], "");
    // Convert the bit string to its decimal number
    return u32::from_str_radix(&intermediate, 2).unwrap();
}

#[aoc(day3, part1)] // 3009600
fn solve_part1(input: &[String]) -> u32 {
    // Create a Vector which can contain a count of each column
    let gamma = most_in(input, None, None);

    // Create a clone of gamma of which epsilon will have ownership, and NOT it
    let epsilon = !gamma.clone();

    bitvec_to_num(gamma) * bitvec_to_num(epsilon)
}

fn most_in(input: &[String], position: Option<usize>, most_common: Option<bool>) -> BitVec<usize> {
    let total_lines = input.iter().len() as u32;
    let mut length = 1;
    if position.is_none() {
        // Create a Vector which can contain a count of each column
        length = input.iter().next().unwrap().len();
    }

    let mut v: Vec<u32> = vec![0; length];

    for line in input.iter() {
        if position.is_none() {
            for (char_index, chr) in line.chars().enumerate() {
                v[char_index] += chr.to_digit(10).unwrap();
            }
        } else {
            v[0] += line
                .chars()
                .nth(position.unwrap())
                .unwrap()
                .to_digit(2)
                .unwrap();
        }
    }

    let mut results: BitVec = BitVec::new();
    for result in v {
        if most_common.is_none() {
            results.push(result > total_lines / 2)
        } else {
            if result == total_lines / 2 {
                results.push(most_common.unwrap())
            } else if most_common.unwrap() {
                results.push(result < total_lines / 2);
            } else {
                results.push(result > total_lines / 2)
            }
        }
    }
    results
}

fn get_rating(input: &[String], most_common: bool) -> Option<u32> {
    let line_length = input.iter().next().unwrap().len();

    // Storage for the vector which we shrink depending on the most common bit
    let mut vec: Vec<String> = input.iter().map(|x| x).cloned().collect();

    for position in 0..line_length {
        if vec.len() == 2 {
            for element in &vec {
                if element.chars().nth(position).unwrap().to_digit(2).unwrap() == most_common as u32
                {
                    return Some(u32::from_str_radix(&element, 2).unwrap());
                }
            }
        } else if vec.len() == 1 {
            return Some(u32::from_str_radix(&vec[0], 2).unwrap());
        }
        // First get the first bit
        let most = most_in(&vec, Some(position), Some(most_common))[0] as u32;
        vec.retain(|line| {
            // Check if the line has our most appearing bit at the position, and retain it
            most != line.chars().nth(position).unwrap().to_digit(10).unwrap()
        });
    }
    return None;
}

#[aoc(day3, part2)]
fn solve_part2(input: &[String]) -> u32 {
    let oxygen = get_rating(input, true).unwrap();
    let co2 = get_rating(input, false).unwrap();
    oxygen * co2
}
