type Mov = (String, u32);

#[aoc_generator(day2)]
fn input_generator(input: &str) -> Vec<Mov> {
    input
        .lines()
        .map(|l| {
            let mut mov = l.split(' ').map(|d| d);
            (
                mov.next().unwrap().to_string(),
                mov.next().unwrap().parse::<u32>().unwrap(),
            )
        })
        .collect()
}

#[aoc(day2, part1)]
fn solve_part1(input: &[Mov]) -> u32 {
    let mut pos = 0;
    let mut depth = 0;

    for mov in input.iter() {
        // Get a full slice so we can match it with &str type
        match &mov.0[..] {
            "forward" => pos += mov.1,
            "up" => depth -= mov.1,
            "down" => depth += mov.1,
            _ => unreachable!(),
        }
    }
    pos * depth
}

#[aoc(day2, part2)]
fn solve_part2(input: &[Mov]) -> u32 {
    let mut pos = 0;
    let mut depth = 0;
    let mut aim = 0;

    for mov in input.iter() {
        // Get a full slice so we can match it with &str type
        match &mov.0[..] {
            "forward" => {
                pos += mov.1;
                depth += aim * mov.1;
            }
            "up" => aim -= mov.1,
            "down" => aim += mov.1,
            _ => unreachable!(),
        }
    }
    pos * depth
}
