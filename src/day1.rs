#[aoc_generator(day1)]
fn input_generator(input: &str) -> Vec<u32> {
    input.lines().map(|l| l.parse().unwrap()).collect()
}

#[aoc(day1, part1)]
fn solve_part1(input: &[u32]) -> u32 {
    let mut count = 0;
    let mut prev = input.iter().next().unwrap(); // Get first element

    for height in input.iter() {
        if height > &prev {
            count += 1;
        }
        prev = height;
    }
    count
}

#[aoc(day1, part2, first)]
fn solve_part2(input: &[u32]) -> u32 {
    let mut count = 0;
    let mut vec: Vec<u32> = Vec::new();
    let initial = input.iter().take(3);
    vec.extend(initial);

    let mut prev_sum: u32 = vec.iter().sum();
    let mut curr_sum: u32;

    for height in input.iter() {
        vec.push(*height);
        curr_sum = vec[vec.len() - 3..].iter().sum();
        if curr_sum > prev_sum {
            count += 1;
        }
        prev_sum = curr_sum;
    }
    count
}

#[aoc(day1, part2, second)]
fn solve_day1_part2_second(input: &[u32]) -> u32 {
    let mut count = 0;
    let mut vec: Vec<u32> = Vec::new();
    let initial = input.iter().take(3);
    vec.extend(initial);

    let mut prev_sum: u32 = vec.iter().sum();
    let mut curr_sum: u32;

    for height in input.iter() {
        vec.drain(0..1);
        vec.push(*height);
        curr_sum = vec.iter().sum();
        if curr_sum > prev_sum {
            count += 1;
        }
        prev_sum = curr_sum;
    }
    count
}
