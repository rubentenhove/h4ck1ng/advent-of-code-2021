use geo::{Coordinate, Line, Point};
use ndarray::prelude::Array2;

#[aoc_generator(day5)]
fn input_generator(input: &str) -> Vec<Line<i32>> {
    input
        .lines()
        .map(|l| {
            let line: Vec<String> = l.split(" -> ").map(String::from).collect();
            let start: Vec<i32> = line[0].split(",").map(|s| s.parse().unwrap()).collect();
            let end: Vec<i32> = line[1].split(",").map(|s| s.parse().unwrap()).collect();
            Line::new(
                Coordinate {
                    x: start[0],
                    y: start[1],
                },
                Coordinate {
                    x: end[0],
                    y: end[1],
                },
            )
        })
        .collect()
}

fn all_points_from_line(input: &Line<i32>, diagonals: bool) -> Vec<Point<i32>> {
    let mut all_points: Vec<Point<i32>> = Vec::new();

    let points = input.clone().points();
    let start = points.0;
    let end = points.1;

    if input.dx() == 0 {
        // Always start from the lowest point as we can't easily have a negative step in a range
        let lowesty = start.y().min(end.y());
        let highesty = start.y().max(end.y());
        for y in lowesty..=highesty {
            all_points.push(Point::new(start.x(), y));
        }
    } else if input.dy() == 0 {
        let lowestx = start.x().min(end.x());
        let highestx = start.x().max(end.x());
        for x in lowestx..=highestx {
            all_points.push(Point::new(x, start.y()));
        }
    } else if diagonals {
        let lowestx = start.x().min(end.x());
        let highestx = start.x().max(end.x());
        let mut y = start.y();
        if !(start.x() == lowestx) {
            y = end.y();
        }
        for x in lowestx..=highestx {
            all_points.push(Point::new(x, y));
            y += input.slope();
        }
    }
    all_points
}

fn count_overlaps(input: Array2<u32>) -> u32 {
    input.iter().filter(|&n| *n > 1).count() as u32
}

#[aoc(day5, part1)]
fn solve_part1(input: &[Line<i32>]) -> u32 {
    let matrix_size = 1000;
    let mut matrix = Array2::<u32>::zeros((matrix_size, matrix_size));
    for line in input.iter() {
        for point in all_points_from_line(line, false) {
            matrix[[point.x() as usize, point.y() as usize]] += 1
        }
    }
    count_overlaps(matrix)
}

#[aoc(day5, part2)]
fn solve_part2(input: &[Line<i32>]) -> u32 {
    let matrix_size = 1000;
    let mut matrix = Array2::<u32>::zeros((matrix_size, matrix_size));
    for line in input.iter() {
        for point in all_points_from_line(line, true) {
            matrix[[point.x() as usize, point.y() as usize]] += 1
        }
    }
    count_overlaps(matrix)
}
