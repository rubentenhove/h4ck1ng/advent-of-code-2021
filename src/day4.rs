use ndarray::prelude::{Array2, ArrayView, Axis};

#[derive(Debug)]
struct Bingo {
    numbers: Vec<u32>,
    cards: Vec<Array2<Option<u32>>>,
    cards_won: Vec<bool>,
}

#[aoc_generator(day4)]
fn input_generator(input: &str) -> Bingo {
    let drawn_numbers = input
        .lines()
        .next()
        .unwrap()
        .split(",")
        .map(|l| l.parse().unwrap())
        .collect();

    let mut card = Array2::<Option<u32>>::default((0, 5));
    let mut cards = Vec::new();

    for line in input.lines().skip(2) {
        if !(line == "") {
            let horline: Vec<Option<u32>> = line
                .split_whitespace()
                .map(|c| Some(c.parse().unwrap()))
                .collect();
            card.push_row(ArrayView::from(&horline)).ok();
        } else {
            cards.push(card);
            card = Array2::<Option<u32>>::default((0, 5))
        }
    }
    cards.push(card);
    let cards_won = vec![false; cards.len()];
    Bingo {
        numbers: drawn_numbers,
        cards: cards,
        cards_won: cards_won,
    }
}

fn check_bingo(input: Array2<Option<u32>>) -> bool {
    for axis in input.axis_iter(Axis(0)).chain(input.columns()) {
        if axis.iter().all(|x| x.is_none()) {
            return true;
        }
    }
    return false;
}

fn sum_board(input: Array2<Option<u32>>) -> u32 {
    let sum = input.iter().fold(
        0,
        |acc, x| if !x.is_none() { acc + x.unwrap() } else { acc },
    );
    sum
}

#[aoc(day4, part1)]
fn solve_part1(input: &Bingo) -> u32 {
    let mut cards: Vec<Array2<Option<u32>>> = input.cards.clone();
    for &drawn in input.numbers.iter() {
        for card in cards.iter_mut() {
            for cardno in card.iter_mut() {
                if !cardno.is_none() {
                    if cardno.unwrap() == drawn {
                        *cardno = None;
                    }
                }
            }
            if check_bingo(card.clone()) {
                return sum_board(card.clone()) * drawn;
            }
        }
    }
    return 0;
}

#[aoc(day4, part2)]
fn solve_part2(input: &Bingo) -> u32 {
    let mut cards: Vec<Array2<Option<u32>>> = input.cards.clone();
    let mut cards_won: Vec<bool> = input.cards_won.clone();
    for &drawn in input.numbers.iter() {
        for (card_index, card) in cards.iter_mut().enumerate() {
            for cardno in card.iter_mut() {
                if !cardno.is_none() {
                    if cardno.unwrap() == drawn {
                        *cardno = None;
                    }
                }
            }
            if check_bingo(card.clone()) {
                cards_won[card_index] = true;
                if !cards_won.contains(&false) {
                    return sum_board(card.clone()) * drawn;
                }
            }
        }
    }
    return 0;
}
