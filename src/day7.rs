#[aoc_generator(day7)]
fn input_generator(input: &str) -> Vec<u32> {
    input.split(',').map(|l| l.parse().unwrap()).collect()
}

#[aoc(day7, part1)]
fn solve_part1(input: &[u32]) -> u32 {
    let highest = *input.iter().max().unwrap();
    let mut best = 999999999;
    for attempt in 0..=highest {
        let mut total = 0;
        for pos in input.iter() {
            total += (*pos as i32 - attempt as i32).abs() as u32;
        }
        if total < best {
            best = total;
        }
    }
    best
}

#[aoc(day7, part2)]
fn solve_part2(input: &[u32]) -> u32 {
    let highest = *input.iter().max().unwrap();
    let mut best = 999999999;
    for attempt in 0..=highest {
        let mut total = 0;
        for pos in input.iter() {
            let mut result = 0;
            let moves = (*pos as i32 - attempt as i32).abs() as u32;
            // Use triangular number
            for i in 1..=moves {
                result += i;
            }
            total += result;
        }
        if total < best {
            best = total;
        }
    }
    best
}
