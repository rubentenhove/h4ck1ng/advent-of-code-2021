use std::collections::HashMap;

#[aoc_generator(day8)]
fn input_generator(input: &str) -> Vec<Vec<Vec<String>>> {
    input
        .lines()
        .map(|l| {
            l.split(" | ")
                .map(|s| s.split_whitespace().map(|s| s.to_string()).collect())
                .collect()
        })
        .collect()
}

#[aoc(day8, part1)]
fn solve_part1(input: &[Vec<Vec<String>>]) -> usize {
    // Work with usizes because len returns usize
    let wanted_amounts: Vec<usize> = vec![2, 4, 3, 7];
    let mut count = 0;
    for line in input {
        count += line[1]
            .iter()
            .map(|c| wanted_amounts.contains(&c.len()))
            .filter(|&n| n == true)
            .count();
    }
    count
}

//  0000
// 1    2
// 1    2
//  3333
// 4    5
// 4    5
//  6666

// Count how many numbers use this segment
fn get_occurance(input: &[String], occurance: u32) -> Vec<char> {
    let mut letters: String = "".to_owned();
    for signal in input.iter() {
        letters.push_str(signal)
    }
    let mut letter_freq: HashMap<char, u32> = HashMap::from([
        ('a', 0),
        ('b', 0),
        ('c', 0),
        ('d', 0),
        ('e', 0),
        ('f', 0),
        ('g', 0),
    ]);
    for letter in letters.chars() {
        // IndexMut was removed from HashMap: https://stackoverflow.com/questions/30414424/how-can-i-update-a-value-in-a-mutable-hashmap
        *letter_freq.get_mut(&letter).unwrap() += 1;
    }
    // Get the chars which occur 'occurance' times
    let mut chars: Vec<char> = Vec::new();
    for (key, val) in letter_freq {
        if val == occurance {
            chars.push(key);
        }
    }
    chars
}

// Get number string based on segment count
fn get_number_segments(input: &[String], segment_count: usize) -> Vec<char> {
    input
        .iter()
        .fold(&input[0], |acc, item| {
            if item.len() == segment_count {
                &item
            } else {
                acc
            }
        })
        .chars()
        .collect()
}

// The other segment than segment 5 in the number 1
fn get_segment_2(input: &[String]) -> char {
    let segment_5 = get_occurance(input, 9)[0];
    let mut number_1 = get_number_segments(input, 2);
    number_1.retain(|&x| x != segment_5);
    number_1[0]
}

// The segment occuring 8 times, other than segment 2
fn get_segment_0(input: &[String]) -> char {
    let segment_2 = get_segment_2(input);
    let mut segments_with_8 = get_occurance(input, 8);
    segments_with_8.retain(|&x| x != segment_2);
    segments_with_8[0]
}

// The segment occuring 7 times and in number 4
fn get_segment_3(input: &[String]) -> char {
    let mut number_4 = get_number_segments(input, 4);
    let segments_with_7 = get_occurance(input, 7);
    number_4.retain(|x| segments_with_7.contains(x));
    number_4[0]
}

#[aoc(day8, part2)]
fn solve_part2(input: &[Vec<Vec<String>>]) -> usize {
    let number_segments: Vec<Vec<usize>> = vec![
        vec![1, 1, 1, 0, 1, 1, 1],
        vec![0, 0, 1, 0, 0, 1, 0],
        vec![1, 0, 1, 1, 1, 0, 1],
        vec![1, 0, 1, 1, 0, 1, 1],
        vec![0, 1, 1, 1, 0, 1, 0],
        vec![1, 1, 0, 1, 0, 1, 1],
        vec![1, 1, 0, 1, 1, 1, 1],
        vec![1, 0, 1, 0, 0, 1, 0],
        vec![1, 1, 1, 1, 1, 1, 1],
        vec![1, 1, 1, 1, 0, 1, 1],
    ];
    let mut sum = 0;
    for line in input.iter() {
        let signals = &line[0];
        let mut segment_letters: HashMap<usize, char> = HashMap::from([
            (0, get_segment_0(signals)),
            (1, get_occurance(signals, 6)[0]),
            (2, get_segment_2(signals)),
            (3, get_segment_3(signals)),
            (4, get_occurance(signals, 4)[0]),
            (5, get_occurance(signals, 9)[0]),
        ]);
        // 6 is just the remaining segment
        let letters: Vec<char> = "abcdefg".chars().collect();
        for letter in &letters {
            if !segment_letters.values().any(|&val| &val == letter) {
                segment_letters.entry(6).or_insert(*letter);
            }
        }
        let mut digits: Vec<usize> = Vec::new();
        for display in &line[1] {
            let mut displayed_number: Vec<usize> = vec![0; 7];
            for (idx, segment) in displayed_number.iter_mut().enumerate() {
                let segment_char = segment_letters[&idx];
                if display.contains(segment_char) {
                    *segment = 1;
                }
            }
            for (idx, number) in number_segments.iter().enumerate() {
                if number == &displayed_number {
                    digits.push(idx);
                }
            }
        }
        sum += digits.iter().fold(0, |acc, elem| acc * 10 + elem);
    }

    sum
}
