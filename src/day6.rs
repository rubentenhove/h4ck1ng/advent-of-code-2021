#[aoc_generator(day6)]
fn input_generator(input: &str) -> Vec<u32> {
    input.split(',').map(|l| l.parse().unwrap()).collect()
}

#[aoc(day6, part1)]
fn solve_part1(input: &[u32]) -> u32 {
    let mut all_fish: Vec<u32> = input.to_vec();
    // We're given the state of day 0 so we can skip that
    for _day in 1..=80 {
        let mut new_fish: Vec<u32> = Vec::new();
        for fish in all_fish.iter_mut() {
            if *fish == 0 {
                *fish = 6;
                new_fish.push(8);
            } else {
                *fish -= 1;
            }
        }
        all_fish.extend(new_fish);
    }
    all_fish.len() as u32
}

#[aoc(day6, part2)]
fn solve_part2(input: &[u32]) -> u64 {
    let all_fish: Vec<u32> = input.to_vec();
    let mut fish_age_count: Vec<u64> = vec![0; 9];
    for age in 0..=8 {
        fish_age_count[age] = all_fish.iter().filter(|&n| *n == age as u32).count() as u64;
    }
    for _day in 1..=256 {
        let creators = fish_age_count[0];
        for age in 1..=8 {
            fish_age_count[age - 1] = fish_age_count[age];
        }
        fish_age_count[6] += creators;
        fish_age_count[8] = creators;
    }
    fish_age_count.iter().sum()
}
